Internet of Things

In the LA 2018 Analytics course IoT featured heavily in the course, in the most recent course it's not even mentioned. Probably no longer needed

AWS IoT allows for secure 2 way communication between AWS and IoT Sensors and the like. AWS IoT allows you to collect ana analyse data from multiple devices and create applications that control these devices.

* Things = devices

# Components
* Broker (which speaks MQTT)
* IOT device gateway (which speaks MQTT)
* IoT SDK != IoT Device SDK. The device SDKs are designed to be used from an embedded device like a pie. Also, to connect to an internet connected "Thing" in AWS, you need to connect from a device SDK

# IoT Device Gateway
* Supports MQTT, WebSockets and HTTP/1.1
* Devices connecting on MQTT & WebSockets maintain a long-lived 2-way connection
* Allows for shifting of infrastructure to AWS to manage it for us

# IoT Thing Registry
* Is used to keep track of all your internet-connected things so you can interact with them effectively.
* Offers CRUD & search/filter
* Things in the registry can be grouped into arbitrary groups you create
## Thing Types
* Allows you to store configuration information that is present across all things of that thing type. E.g.: All temperature sensors would have a tollerance, a time to change (as in how quickly it updates), how much volt & watt it consumes etc. Think of these as tag keys with different values per thing.
* Note that things in your registry can only have <=3 attributes. If you have them in a thing type you can attach <=50 attributes to them.
* A thing can only be associated with 1 thing type
* There is no limit to the number of thing types you can have.
## Thing Groups
* Allows CRUD on group permission
* Manage permissions over multiple things at once by attaching policies to a group
* Allows to configure logging at the group level

# IoT Authentication & Authorization
## Depending on device
* X.509 certs (Things)
  * Supports CSRs or one-click cert creation where AWS has your private key
* Cognito (Mobiles/Browsers)
* IAM (SDKs)
##
* You attach an (IAM) policy to a certificate! (probably also a group)
* You attach the cert to an IoT Thing

# IoT Message Broker (Broker)
* Sensors publish to the IoT Topic, subscribers pick it up.
* The service that provides this pub/sub facilities is called a broker
* Topics are namespaces like: sensor/light/livingroom or /sensor/temperature/livingroom
* Some topics exist by default and are reserved for lifecycle events and the device shadow service
  * Those are Connect, Disconnect, Subscribe and Unsubscribe events.
* MQTT is the standard protocol, lots of IoT Things can talk MQTT
* It also supports HTTPS over IPv4 & IPv6
* MQTT is also supported over webSockets.
## Authentication methods
-----------------+-------------+----------
Protocol         | Auth method | Port
-----------------+-------------+----------
MQTT             | Client Cert | 8883, 443
HTTP             | Client Cert | 8883, 443
HTTP             | SigV4       | 443
MQTT + WebSocket | SigV4       | 443

# IoT Rules
* A way to look out for a certain message from a certain sensor or certain type of update from a group
* You write them in SQL (following the AWS IoT SQL Reference/dialogue)
* Once a set of events occurs, I can react in a certain way: invoke lambda, trigger a cloudwatch alarm state, store message in a S3 bucket, Add msg to DynamoDB, Republish it to a different topic, trigger a step function, place message on a Kinesis stream
## Basic Ingest
* Publishing a message on a MQTT topic costs money
* Basic Ingest allows your sensor to publish an update straight to $aws/rules/rules-name avoiding the message cost
* Important since it significantly reduces the cost.

# IoT Device Shadow Service
* IoT Suite maintains IoT Shadows (represntation of the physical device)
* Every time an IoT Device updates AWS IoT, this IoT Shadow gets updated
  * This is because IoT devices aren't always online but you want to know their last update
  * Equally, if you need to update a device you can write to the IoT Shadow and the device will be updated once the device checks in with the Device Gateway again
  * This allows for asynchronous communication
## Shadow Device Document
* Is written in fully compliant JSON
* A shadow document looks like:
```
{
    "state": {
        "desired": {
            "attribute1": integer2,
            "attribute2": "string2",
            ...
            "attributeN": boolean2
        },
        "reported": {
            "attribute1": integer1,
            "attribute2": "string1",
            ...
            "attributeN": boolean1
        }
    },
    "clientToken": "token",
    "version": version
}
```
* The shadow device document has a state for each devive, made up of a "desired" and a "reported" state
* It has a clientToken which, if used, is a unique string to associate MQTT requests and responses
* And it has Version, if used, the shadow service only processes the update if the specified version matches the latest version it has.
## MQTT Topics for interacting with shadows
* The shadow service uses broker topics to publish and receive information relating to interacting w/ device shadows.
* These topics follow this naming convention: $aws/things/thingName/shadow/action
  * Actions are /update, /update/accepted, /update/documents, /updte/rejected, /update/delta
  * /get, /get/accepted, /get/rejected
  * /delete, /delete/accepted, /delete/rejected
  * i.e.: '$aws/things/car1/shadow/get'

# AWS IoT Job Service
AWS Iot Jobs are a set of operations sent to and executed on AWS IoT devices. Think updates, troubleshooting, certificate rotation etc
## Key Concepts
* Job document -> A UTF-8 encoded JSON doc containing all necessary information, usually a URL to pull more information from
* Target -> A specific thing [group] that should perform the job
* Job execution -> An running job on a target. Targets report their process to AWS IoT
* Snapshot Job -> A type of job that runs once for all targets (think ECS Task)
* Continuous Job -> A type of job that continuous to run and will also be run on new devices added to the target group (think ECS Service)
* Rollouts -> A way to say execute only X many jobs per minute, from 1 to 1000/minute
* Pre-signed URLs -> The AWS IoT Job Service replaces placeholders with actual pre-signed URLs for time-limited S3 access, e.g.: ${aws:iot:s3-presigned-url:https://s3.amazonaws.com/bucket/key}

# Working with the SDKs
* You can allow the device to queue infinite messages
  `IoTclient.configureOfflinePublishQueueing(-1)`
* You can set a number of messages to drain from that queue once the Thing reconnects. Make sure that this number is bigger than the concurrent requests you normally receive, otherwise your queue will never drain completely.
  `IoTclient.configureDrainingFrequency(2)  # 2 requests/second`
* You can set a timeout for [dis]connects to occur (in seconds)
  `IoTclient.configureConnectDisconnectTimeout(10)`
* And lastly, set a timeout for publish/[un]subscribe (in seconds)
  `IoTclient.configureMQTTOperationTimeout(5)`
* It's then common practice to publish a "hello" once connected:
  ```
  IoTclient.connect()
  IoTclient.publish(TOPIC, "connected", 0)
  ```

# AWS Greengrass
extends AWS Cloud capabilities to local devices. This allows those devices to collect and anlyse data close to the source and communicate securely within the LOCAL network. You can also use it to deploy Lambda functions to local devices! AWS Greengrass is still AWS IoT however does not fall under AWS IoT Core (it's its own console service but under the IoT interface)

## Greengrass groups
are a collection of settings for AWS Greengrass core devices and the devices that communicate with them.
* Greengrass group definition -> A definition that contains a collection of information about the AWS Greengrass group (duh)
* Greengrass group settings -> Settings for a Greengrass group including an AWS Greengrass group role, log config, certificate and connection information, and AWS Greengrass Core connectivity information
* Greengrass Core -> An IoT Thing that represents the Greengrass Core. See below.
* Lambda function definition -> A lambda function to be deployed in AWS Greengrass Core will need one of these definitions.
* Subscription definition -> This definition contains a list of subscriptions that enable communication with MQTT messages and contains:
  * A message source -> An AWS IoT Thing, Lambda Function, AWS IoT or local AWS Greengrass shadow service. This is similar to the shadow service in AWS IoT but makes it possible to run a Lambda function locally without connecting to the cloud.
  * A subject -> An MQTT topic or topic filter that's used to filter message data.
  * A message target -> Identifies the destination for messages and can be any of the things the message source can be.
* Device Definition -> A list of devices that are members of the Greengrass Group with their configuration data.
* Resource Definition -> A list of local resources and machine learning resources on the AWS Greengrass Core and configuration data.
## Greengrass Core
* There are 2 device types, AWS Greengrass Cores and IoT devices connected to the Greengrass core.
* Every Greengrass Group requires a Core device running the Greengrass software (locally) called the runtime.
It enables communication between devices, local Lambda functions and the cloud.
* The group itself contains a Core & other devices
* The core device:
  * Is itself an IoT device.
  * Has it's own certificates for authenticating with AWS IoT.
  * Have a device shadow and exist in the IoT registry.
  * Run a local AWS Lambda runtime.
  * Run other software to allow AWS IoT devices to find their group and core connection information.
### AWS IoT Device
* Non-core device can connect to the Greengrass cores.
* They run software written with the AWS IoT Device SDK
* Can be things like sensors and actuators
* Can function ourside of an AWS Greengrass Group
