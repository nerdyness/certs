# Neptune facts
* Fully managed GraphDB
* Supports open graph APIs for both "Apache TinkerPop Gremlin" (or Gremlin for short) & SPARQL
* Highly Available, At least 6 copies of your data spread across 3 AZs (more similar to Aurora than RDS, it's a "cloud volume" with a compute cluster around it)
* Up to 64TB storage
* Up to 15 read replica
* Snapshots/restore
* Encryption at rest via KMS CMK
* Popular use case:
  - social networks/relationships
  - knowledge graphs
  - fraud detection (tracking behaviour of people & fraudsters)
  - recommendation engine (people who bought this also smell their own farts)
# Gremlin vs SPARQL
## Gremlin
* Graph structure Property -> ```
g.V().has('name','location','WA').out('location').values('name','location')
  [name: John, location: WA]
  [name: Jane, location: WA]
```
* Interface: WebSockets
* Query Pattern: Traversal
## SPARQL
* Graph structure: RDF - Resource Definition Framework -> ```
SELECT ?person ?location
WHERE {
  <WA> ?location ?person .
}
  [name: John, location: WA]
  [name: Jane, location: WA]
```
* Interface: HTTP REST
* Query Pattern: SQL
