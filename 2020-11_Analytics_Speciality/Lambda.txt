* No servers
* Continuous scaling
* Dirt cheap!!
* Every time a Lambda function is triggered (e.g. by HTTP request) you're invoking a new Lambda function. Each function is a duplicate, like a container is a duplicate of an image.
* 1,000,000 free requests per month, after that $0.20 per 1,000,000 requests
* You're also billed on duration. When your code to executes to when it terminates, rounded to the nearest 100ms. The price for the duration changes depending on how much memory you've got allocated. You are charged $0.00001667 for every "GB-second" used.

# Versioning
You can publish multiple versions of the same Lambda function. After you publish a version it is immutable and can not be changed. The only version that is mutable is $LATEST which always points to the latest version of your function.

You can get the version by adding it to the arn
arn:aws:lambda:aws-region:account-id:function:functionname:$LATEST
If you add the version, it's a qualified arn, an unqualified arn is the same without a version:
arn:aws:lambda:aws-region:account-id:function:functionname

With versioning you can create an alias that points to a specific version, let's say your version is 2.1.4, the alias for that could be "prod".
So first you create your lambda, then publish it as version 1.0 or whatever, then create an alias for version 1 called "prod". If $LATEST changes, it won't impact the prod version of your lambda.
With versioning you can route a percentage of your traffic to a new Lambda function (80% to the old version, 20% to the new version)

# EXAM
* Know your Lambda triggers:
  - API Gateway
  - AWS IoT
  - Alexa Skills Kit
  - Alexa Smart Home
  - CloudFront
  - CloudWatch Events
  - CloudWatch Logs
  - CodeCommit
  - Cognito Sync Trigger
  - DynamoDB Streams
  - Kinesis
  - S3
  - SNS
  - SQS !!!
* Lambda scales out (not up) automatically
* Lambda functions are independent, 1 event = 1 function
* Lambda is serverless!
* You need to know what other services are serverless (S3 but not EC2 for instance)
* Lambda functions can trigger other functions, thus 1 event = n functions
* The maximum threshold is 5 minutes. You'd have to break down the function and make it call the next one. Max is 15 minutes. Min is 1 sec.
* Architecture can get extremely complicated, thus AWS X-Ray can help to debug.
* Supported languages:
  - Node.js
  - Java
  - Python
  - C#
  - Go
  - Ruby!! :)
* Lambda can do things globally, you can use it to back up S3 buckets to other buckets
* Lambdas can have multiple versions
* Latest version is $LATEST
* Qualified vs unqualified arns
* Versions are immutable
* You can split traffic between two versions (but not $LATEST)
