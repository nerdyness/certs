Simple Workflow Service is a web service that makes it easy to coordinate work across distributed application components. Amazon SWF enables applications for a range of use cases, including media processing, web application back-ends, business process workflows, and analytics pipelines, to be designed as a coordination of tasks.

Tasks represent invocations of various processing steps in an application which can be performed by executable code, web service calls, human actions and scripts.

The workers and the deciders can run on cloud infrastructure such as EC2, or on machines behind firewalls. SWF brokers the interactions between workers and the decider. It allows the decider to get consistent views into the progress of tasks and to initiate new tasks in an ongoing manner.

SWF stores tasks, assigns them to workers when they're ready and monitors their progress. It ensures that a task is assigned ONLY ONCE and is never duplicated. Since SWF maintains the application's state durably, workers and deciders don't have to keep track of execution state. They can run independently and scale quickly.

# SWF Domains
Your workflow and activity types and the workflow execution itself are all scoped to a domain. Domains isolate a set of types, executions and task lists from others within the same account.

You can register a domain by using the mgmt console or by using the RegisterDomain action in the SWF API.

# SWF vs SQS
* SWF presents a task-oriented API, whereas SQS offers a message-oriented API
* SWF ensures a task is assigned only once, never duplicated. SQS you need to handle duplication and your message may be processed more than once.
* SWF keeps track of all tasks & events in an app. With SQS you need to implement you own app-level tracking, especially if you use multiple queues.

# EXAM
* In a workflow you can have deciders and workers.
* A worker is a program that interacts with SWF to get tasks, process received tasks and return results.
* A decider is a program that controls the coordination of tasks, i.e.: their ordering, concurrency and scheduling according to the application logic.
* The workflow retention period determines how many days a workflow will be retained for. The maximum value is 1 year and it's measured in seconds.
* These SQS vs SWF questions can be tough, do you need human interaction? Then SWF. Is the timeframe ~12h or longer? Less than 12h SQS, More SWF.
