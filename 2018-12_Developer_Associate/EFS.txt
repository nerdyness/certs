* Elastic File System, EFS adopt to capacity automatically, growing and shrinking as needed.
* Can be mounted to multiple locations at once
* Supports NFSv4 and NFSv4.1
* You only pay for the storage you use, 30 cents/gig
* Scales to Petabytes
* supports thousands of concurrent connections
* Stored/replicated across multiple AZs in a region
* Read after Write Consistency. As soon as you put a new object on EFS you will be able to read from it.
* You can turn on lifecycle policies which migrates files that have not been accessed for 30 days to the EFS IA storage class
