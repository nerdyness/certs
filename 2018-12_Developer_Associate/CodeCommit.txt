CodeCommit is a fully managed source control service (private repos in AWS) that enables companies to host secure and scalable private git repos

# EXAM
* Centralised repos
* Tracks and manages changes
* Maintains versions
* Manages updates from multiple sources
* Enables collaboration :D

* Branching
* Commits
* Merges
* Data is encrypted in transit and at rest!
