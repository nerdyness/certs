STS
Grants users limited and temporary access to AWS resources. Users can come from three sources:

* Federation (typically AD)
  * Uses SAML (Security Assertion Markup Language)
  * Grants temporary access based off the user's AD credentials. Does not need to be an IAM user.
  * SSO allows users to log in to AWS console without assigning IAM credentials
* Federation with Mobile Apps
  * Use FB/Amazon/Google or other OpenID providers to log in.
* Cross Account Access
  * Let's users from one AWS account access resources in another

Terminology:
Federation: combining or joining users from different IdP
Identity Broker: A service that allows you to take an identity from point A and join it (federate it) to point B
Identity Store: IdP like FB, AD, Google etc
Identities: A (multiple) users


There was this scenario in the video, apparently for the exam we need to know:
1. Develop an Identity Broker to communicate with LDAP and AWS STS
2. Identity Broker always authenticates with LDAP first, THEN with STS
3. Application then gets temporary access to AWS resource

OR:
1. Develop an Identity Broker to communicate with LDAP and AWS STS
2. Identity Broker always authenticates with LDAP first, THEN gets an IAM Role associated with a user
3. Application then authenticates with STS and assumes that IAM Role (NOT CREDENTIALS)
4. Application uses that IAM role to interact with S3
3. Application then gets temporary access to AWS resource
