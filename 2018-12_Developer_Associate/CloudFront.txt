Is a CDN
A CDN is a system of distributed servers (network) that delivers web content to a user based on the geographic locations of the user, the origin of the webpage and the content delivery server?! // FIXME

CloudFront can be used to deliver your entire website, including dynamic, static, streaming and interactive content using a global network of edge locations. Requests for your content are automatically routed to the user's nearest edge location to deliver content with the best possible performance.

Amazon CloudFront is optimised to work with other AWS Services, like S3, EC2, ELB, Route53. CloudFront also works seamlessly with non-AWS origin servers, which store the original, definitive versions of your files.

# EXAM
- EdgeLocation: is a location where content will be cached. (Different to a region/AZ)
- Origin: is the source of all data the CDN can distribute, in CloudFront this can be any combination of, an ELB, EC2 instance, S3 bucket, Route53 or non-AWS origin servers.
- Distribution: is the name for a collection of edge locations.
  - "Web" Distribution is used for websites (files?)
  - "RTMP" - Used for Media Streaming (Real-Time Messaging Protocol)
  - You can have multiple origins per Distribution.
- Edge locations aren't read only, you can put an object into them as well to make uploads etc a lot faster.
- Content stays at the edge location for the specified TTL (in seconds).
  - You can clear cached objects though you will be charged for it
- You can use pre-signed URLs or pre-signed cookies to restrict viewer access (only allow access from udemy.com for instance). Very frequent in the Exam!
- You can use Geo Restrictions, needs to be a white-list or a black-list of countries.
