DynamoDB is a fast and flexible NoSQL database service for applications that need consistent, single-digit millisecond latency at any scale. It's fully managed and supports both document and key-value data models. It's flexible data model and reliable performance make it a great fit for mobile, web, gaming, ad-tech, IoT, and many other apps.

# Data model
Table
Items ~ Similar to a row of data in SQL
Attributes ~ A column in a table

Attributes can be nested, i.e.: attribute address has attributes 'street name' & 'number'. DynamoDB supports up to 35 levels of nesting.

# Pricing
* Provisioned Throughput Capacity
  * Write throughput, $0.0065/hour for every 10 units
  * Read  throughput, $0.0065/hour for every 50 units
* Data
  * First 25GB stored are free
  * $0.25/GB/months for anything >25GB

# Primary keys
You can have a single attribute primary key called a partition key or a composite key, composed of a partition key and a Sort Key.
* Partition key (used to be called hash key): contains a Single Attribute (i.e. unique ID) and can only contain 1 attribute!
  - DynamoDB uses the partition key's value as input for an internal hash function, the output determines the physical partition/location on which the data is stored.
  - It's a hash function, no 2 inputs can have the same output
* Composite (i.e. a unique ID and a date range): Partition key and sort key (used to be called range key) composed of 2 attributes.
  - While 2 items can have the same partition key, they must have a different sort key
  - All items with the same partition key are stored together, in order of the sort key value.
  - In an RDBMS you can create a combined primary key out of various columns, this is different, it's 1 attribute or 2 attributes, not an arbitrary number.

# Indexes
* Local Secondary Index
  - Same partition key, Different sort key.
  - Can ONLY be created when you create a table.
* Global Secondary Index
  - Has a Different partition key and different sort key.
  - Can be created when you create a table OR LATER.

# Streams
Used to capture any kind of modification of the DynamoDB tables.
* If a new item is added to the table, the stream captures an image of the entire item, including all of it's attributes.
* If an item is updated, the stream captures the before & after "image" of any attributes that were modified in the item.
* If an item is deleted from the table, the stream captures an image of the entire item before it gets deleted.
* Streams are only stored for 24hours and will then be discarded
* You can trigger Lambda functions from streams (this is how DynamoDB triggers Lambdas)
* Streams can be disabled and aren't enabled by default (I think)

# Query vs Scan
## Query
* A query returns an item from the table using only primary key attribute values. You must provide a partition attribute name and a distinct value to search for (id = 23894)
* You can optionally provide a sort key attribute name and value, and use a comparison operator to refine your search results (where time > 2018)
* By default, a query returns all attributes of the matching item(s), however you should limit which parameters are being returned using the ProjectionExpression parameter.
* Query results are ordered by the sort key in ascending order (lowest number first, a before b etc), if you want to reverse that set the ScanIndexForward parameter to false.
* By default, a query is going to be eventually consistent. If you need strongly consistent queries you can change that in the query.
## Scan
* A Scan returns all attributes for every item (dumps the table) though you should limit the results using the ProjectionExpression parameter.
* A Scan operation will always scan the entire table and then filter out what ain't needed, saving a little bit but not much performance, use a query whenever you can. You could totally max out your DynamoDB Read Capacity Units by triggering a scan operation.
* Design your tables to have clever indexes, so you can use Get, or BatchGetItem API calls instead of scans.

# Provisioned Throughput
* All reads are rounded up to increments of 4KB
* Eventually consistent reads (default) consist of 2x4KB reads/second
* Strongly consistent reads consist of 1x4KB read/second
* All writes are rounded up to 1KB
* All writes consist of 1 write/second
* Read/write throughput needs to be an integer >1
You have an application that requires to read 5 items of 10KB/second using strong consistency. What should you set the read throughput to?
"Magic Read Formula"
Size you need to read rounded up to the nearest 4KB chunk / 4KB (to get # of "units of read throughput") * # of items = read throughput for strong consistency. (Divide by 2 for eventual consistency)

10 items, 6KB, eventual consistency: 8KB / 4KB * 10 /2 = 10
5 items, 10KB, eventual consistency: 12KB/4KB =3 * 5 /2 = 8
5 items, 10KB, strong consistency:   12KB/4KB =3 * 5 = 15

Reading 300 items every 30 seconds, each item 5kb, eventually consistent. -> 10 items/second w/ 5kb -> 8KB / 4KB * 10 /2 = 10

# On-demand capacity (probably not in the exam at all)
* With on demand AWS figures out how much throughput is required and throttles your instance accordingly
* DynamoDB instantly scales up/down based on the current activity
* It's free but if you have a very predictable workload then the provision capacity (NOT ON-DEMAND) can be cheaper

# Transactions (probably not in the exam at all)
* ACID Transactions (Atomic, Consistent, Isolated, Durable)
* Read/write multiple items across multiple tables as an all or nothing operation
* Check for a pre-requisite condition before writing to a table

# TTL - Time to live
* Defines an expiry time for your data (in epoch)
* Expired items will be marked for deletion and removed within 48h
* Because 48h is a huge window, consider filtering out expired items
* Great for removing old data such as session data, event logs, temp stuff
* Reduces cost

# EXAM
* DynamoDB consists of Tables, Items and Attributes
* Supports both document and key-value data models
* Supports document formats: JSON, HTML, XML
* 2 Types of primary keys, partition key and a combination of partition key and sort key (called composite key)
* 2 Consistency models:
  * strongly consistent
  * eventually consistent
* Access is controlled using IAM policies
* Fine grained access control using IAM condition parameter `dynamodb:LeadingKeys` to allow users to access only the items that where the partition key matches their user id.
## Indexes
* Indexes enable fast queries on specific data columns
* Give you a different view of your data based on alternative partition/sort keys
* Important to know differences between Local Secondary Index and Global Secondary Index
## Scan vs Query
* A Query operation finds items in a table using only the primary key attribute
* You provide the Primary Key name and a distinct value to search for
* A scan operation examines every item in the table
  * Returns all attributes by default
  * Use the ProjectionExpression parameter to refine the results
* Query results are always sorted by the sort key (if there is one)
  * In ascending order
  * Set ScanIndexForward parameter to false to reverse the order for QUERIES only
* Query operations are generally more efficient than a scan
* Reduce the impact of a query or scan by setting a smaller page size which uses fewer read operations
* Isolate scan operations to specific tables and segregate them from your mission-critical traffic
* Try parallel scans rather than the default sequential scan
* Avoid using scan operations if you can: design your tables in a way that you can use the query, get or BatchGetItem API
## Throughput
* Provisioned Throughput is measured in capacity units
* 1 write capacity unit = 1KB/s
* 1 read capacity unit = 4KB/s Strongly consistent Reads
  1 read capacity unit = 8KB/s Eventually consistent Reads
### Magic Formula - write
* First caluclate how many capacity units you need for each WRITE
  size of each item/1KB
* Roundup to the nearest whole KB/s WRITE
* Multiply by the number of WRITES per second
### Magic Formula - read
* First caluclate how many capacity units you need for each READ
  Size of each item/4KB
* Roundup to the nearest whole multiple of 4KB/s READ
* Multiply by the number of READS per second for strongly consistent reads
  Divide that number (of capacity units) by 2 for eventually consistent reads
## DAX - DynamoDB Accelerator
* Provides in-memory caching for DynamoDB tables
* Improves response times for EVENTUALLY consistent reads ONLY
* You point Your API calls to the DAX cluster instead of your table
* If the item you are querying is on the cache, DAX will return it; otherwise it will perform an eventually consistent GetItem operation on your DynamoDB Table
* Not suitable for write-intensive applications or applications that require strongly consistent reads.
