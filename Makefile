EXAM=2020-11_Analytics_Speciality
FILE=Redshift.txt

.PHONY: help
help: ## Prints this help/overview message
	@echo "Remember to change EXAM=$(EXAM) and FILE=$(FILE) to whatever you want to review"
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z0-9_-]+:.*?## / {printf "\033[36m%-13s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.PHONY: show
show: ## Show a given FILE and refresh it on change
	# sleep .3  ; open "http://localhost:6419/"
	grip --wide ./$(EXAM)/$(FILE)

.PHONY: show_all
show_all: ## Show all files of one EXAM
	for i in $(EXAM)/*; do make show EXAM=./ FILE=$$i; done

.PHONY: next
next: ## Kills all grip servers running, handy for show_all
	ps auxw | grep \/grip | grep -v grep | awk  '{print $$2}' | xargs kill

.PHONY: dependencies
dependencies: ## Install all dependencies
	brew install grip

#.SILENT:
