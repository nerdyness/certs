# EXAM
* All inbound traffic is blocked by default, we can't set a deny rule, only allow
* All outbound traffic is allowed per default.
* Updates to security groups are always applied immediately
* Security Groups are stateful, established connections can go out even if you delete the default all outbound rule
* You can have any number of EC2 instances in a security group
* You can have multiple (<=5) SGs attached to an instance.
* If you want to block a certain IP (range) you need to use NACLs, there is no "deny" in SGs.
