# Kinesis
is a platform on AWS you can send your streaming data to. Kinesis makes it easy to load and analyze streaming data, and also providing the ability for you to build your own custom application for your business needs.

Data producers are EC2 instances, IoT Devices, mobile phones, traditional computer hardware etc. They send the data into Kinesis where it can be processed and stored.

# Streaming Data
is data that is generated continuously by 1000s of data sources, which typically send in the data records simultaneously, and in small sizes (order of kilobytes).

Like
* purchase records on amazon.com
* Stock market data
* Game data
* Social networking data
* Geospatial data (uber.com)
* IoT sensors

# Core kinesis services are:
## Kinesis Streams
* Video Streams - securely stream video from connected devices to AWS for analytics and machine learning
* Data streams - Build custom applications and process data in real-time.
* Kinesis stores this data, default 24h max 7 days. The data is stored in "shards".
* Consists of shards which can each read 5 transactions per second at a total data read rate of 2MB/second and up to 1,000 records per second for writes, with a maximum total data write rate of 1MB/second (including partition keys). You don't need to know these figures!
* The data capacity stream is based on the number of shards
* There is a fleet of EC2 instances which receive the data from the shards, they're called data consumers.
## Kinesis Firehose
* Allows you to capture, transform and load data streams into Kinesis Firehose for near real-time analytics with Business Intelligence Tools
* Data producers send data to Firehose
* You don't have to worry about shards or sharding to keep up with your demand, it's completely automated.
* You can analyse the data in real-time using Lambdas and store the result in S3 or ElasticSearch
* Analysis is completely optional
* No data retention! As soon as the data comes in it's either processed right away or stored right away
* Storing data in RedShift means you will have to store that data in S3 first before it can be imported into RedShift
## Kinesis Analytics
* Allows you to run SQL queries on that data as it exists in Kinesis Streams or Kinesis Firehose
* You can use that query to store the data in S3, Redshift or Elasticsearch Cluster

# EXAM
* Know the difference between the 3 different core services (you'll be given examples and if it talks about shards, you know it's going to be Streams, if it talks about analysis using Lambda without the need to store anything you know it's Kinesis Analysis etc).
 Understand what Kinesis Analytics is.
