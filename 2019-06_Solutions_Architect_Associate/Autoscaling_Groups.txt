# Launch configuration
* Pick AMI, instance size, IAM role, User Data, IP Address Type, Storage, Security Group, KeyPair etc.

* An ASG needs a launch configuration to know how to create a new instance if an event occurs
* The launch configuration is called by the ASG (basically 2 separate services however an ASG can't exist without an LC but you can have an LC on it's own)
* Can trigger an SNS notification when it scales.
* When you delete an ASG all the instances it created will be deleted as well.
