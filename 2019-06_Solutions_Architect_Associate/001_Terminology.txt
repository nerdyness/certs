Availability
* The system is up, it's available but it might perform in a degraded state.
Reliability
* Reliability is closely related to Availability, however a system can be 'Available' but not be working properly. Reliability is the probability that a system will work as designed. This term is not used much in AWS, but is still worth understanding
Durability
* Durability refers to the on-going existence of the object or resource. Note that it does not mean you can access it, only that it continues to exist. Think of Amazon S3 and the 11 x 9s of durability.
Resilience
* The ability of a workload to recover from infrastructure or service disruptions, dynamically acquire computing resources to meet demand, and mitigate disruptions, such as misconfigurations or transient network issues. [ https://wa.aws.amazon.com/wat.concept.resiliency.en.html ]
Fault-tolerance
* Is a higher requirement than high availability - it conceals it's failures from the user.
# RTO & RPO
## Recovery Time Objective
* How long does it take for the system to recover and come online again?
## Recovery Point Objective
* How much data is lost if the system fails?
