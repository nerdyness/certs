Web Identity Federation lets you give your users access to AWS resources after they have successfully authenticated with a web-based identity provider like Amazon, Facebook or Google.

Following successful authentication the user receives an authentication token from the Web IdP which they can trade for temporary AWS security credentials.

# Cognito
Amazon Cognito provides Web Identity Federation with the following features:
* Sign-up and sign-in to your apps
* Access for guest users
* Acts as an Identity Broker between your application and the Web IdP, so you don't have to write any additional code.
* Synchronises user data for multiple devices.
* Recommended for all mobile applications which run on AWS Services.

It is the recommended approach for Web Identity Federation using social media accounts like FB.

Cognito brokers between the app and FB or Google to provide temp credentials which map to an IAM role allowing access to the required resources. No need for application embedded AWS credentials that get stored on the users device, seamless experience across all mobile devices.

# Cognito User Pools
are user directories used to manage sign-up and sign-in functionality for mobile and web applications.

Users can sign-in directly to the UserPool, or indirectly via an IdP. Cognito acts as an Identity Broker between the IdP and AWS. Successful authentication generates a number of JSON web tokens (JWTs).

Identity pools enable you to create unique identities for your users and authenticate them with identity providers. With an identity, you can obtain temporary, limited-privilege AWS credentials to access other AWS services.

Cognito User Pool Example
1. User signs in via FB and receives JWTs
2. Cognito exchanges the JWT for temporary AWS access credentials
3. You can access AWS with that.

# Push synchronisation
Cognito tracks the association between user identity and the various different devices they sign-in from.
In order to provide a seamless user expereince for your app, Cognito uses Push Synchronisation to push updates and synchronise user data across multiple devices.
Amazon SNS is used to send a silent push notification to all the devices associated with a given user identity whenever data stored in the cloud changes.

# EXAM
* Federation allows users to authenticate with a web IdP
# Web Identity Federation Workflow
1. User signs into FB
2. FB gives user a web token
3. Cognito will exchange that token into temporary AWS credentials
4. With that token the user can access AWS/assume an IAM role

* Cognito brokers between the IdP and your app, you don't have to write any code to do this.
* It provides sign-up, sign-in and guest user access
* Syncs user data for a seamless experience across the user's devices
* Cognito is the AWS recommended approach for Web ID Federation particularly for mobile apps

* Cognito uses user pools to manage user sign-up and sign-in directly or via an IdP
* Identity pools authorise access to your AWS resources.
* Cognito acts as an Identity Broker, handling all interaction with the IdP
* Cognito uses Push Synchronisation to send a silent push notification of user data updates to multiple device types associated with a user ID
