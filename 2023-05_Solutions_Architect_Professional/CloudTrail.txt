# What gets logged?
Metadata around API calls such as
* Identity of caller
* Time of call
* source (IP address) of caller
* request Parameters
* response of the service

# EXAM
* CloudTrail is turned on by default (now) in any new AWS Account
  * You can see all events for the past 90 days, not in an S3 bucket
  * You pay for the storage for S3 if you choose to copy the data to S3
  * First trail is free in each account, every other trail costs $2 per 100,000 mgmt events
* CloudTrail logs are sent to S3
* You can have many different trails with different configurations (i.e.: read-only events, write events, only certain regions etc)
  * You can then easily have duplicate events because each trail gets the event it's configured for, 2 identical trails will log the identical data to the destinations.
* You can configure an "All region" Trail and tick "deploy in all accounts in this organization" (from the parent (formerly master) account)
* You can enable "data events" logging which is highly recommended since this is the glue that makes CW Events work
* Trails can be KMS encrypted
* You manage retention via life cycle policies, otherwise stored forever
* Delivered every 5 minutes with <= 15 minute delay ONLY if there is an active log
* SNS notifications are available
* You can do cross-region/cross-account aggregation into a single security account.

## Protect your logs
* Security folk get CloudTrailAdmin, Auditors get the CloudTrailReadOnly role
* Restrict access to the S3 bucket via BucketPolicies & MFA
* Archive to Glacier for long-term keepin'
* Check integrity via digest files
* You want to protect your CloudTrail logs because they can contain PII (personal identifiable information). Security account

## Log file validation
* You can turn on log file validation so for every log file it puts out a signed hash so you can see that it hasn't been tampered with. Every hour CloudTrail will place a signed file into S3 showing what log files it wrote. The digest files take an hour to produce as well.
* The hash is signed with Amazon's private key and you can verify it with their public key (SHA256+RSA keys)
* Validation can be triggered via SNS as soon as the file is placed there. Although, chances are if somebody was to tamper with the logs that would happen way after the file was placed there so maybe do it periodically via a Lambda
