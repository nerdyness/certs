AWS Shield is a free service that protects all customers on ELB, CloudFront and Route53.

* Protects against SYN/UDP floods, reflection attacks, and layer3/4 attacks.
* AWS Shield Advanced provides enhanced protection for EC2, NLB, ELB, CloudFront and Route53 (This is different to just Shield. Shield can't do EC2 nor NLB!!! You enable Shield Advanced on an EIP you attach to EC2/NLB).
  * dedicated DDoS Response Team (DRT), 24x7 availability to manage and mitigate application layer DDoS Attacks.
  * Always-on flow-based monitoring of network traffic and active application monitoring to provide near real-time notifications of DDoS attacks.
  * Protection against increased AWS bill due to DDoS Scale Up on ELB, CloudFront and Route53
  * Shield Advanced is $3k/month

# EXAM
* https://d1.awsstatic.com/whitepapers/Security/DDoS_White_Paper.pdf (if you have time, not required)
* Technologies you can use to mitigate a DDoS attack;
  * CloudFront
  * Route53
  * ELB's
  * WAFs
  * AutoScaling (both for WAF and EC2)
  * CloudWatch (for alerting)
