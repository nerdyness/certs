Web Application Firewall (Layer 7 Firewall)

Monitors HTTP/S requests that come via CloudFront, API GW or ALBs. It can also control access to your content based on conditions such as source IP. Other conditions include what query string parameters need to be present on the request and WAF can return a 403 Forbidden at this point.
Application aware Layer 7 firewall.

# 3 behaviours
* Allow all but matched requests
* Block all but matched requests
* Count requests that match some WAF policy

# Properties to work with
* Source IP
* Source IP Country
* Values in HTTP/S request headers
* Regex matches against the entire request
* Length of request
* Presence of SQL (SQL injections)
* Presence of scripts (XSS attacks)

# EXAM
* WAF integrates with ALB, CFront, API Gw
* Can't use it with ELB or NLB
* You string together conditions into rules. Then you create a (Web) ACL which contains a collection of these rules (0..n) and you have a Default Action/Catch-all at the end for everything that hasn't been caught by a prior rule
