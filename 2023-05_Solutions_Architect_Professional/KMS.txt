KMS Key Management Service - Is a managed service that makes it easy for you to create & control the encryption keys used to encrypt your data. It's included with services such as EBS, S3, RedShift, Elastic Transcoder, WorkMail, RDS and others to make it simple to encrypt your data with keys that you manage (via KMS).

KMS is part of IAM however it's a regional service (not global). Keys from one region can't be used to encrypt/decrypt data in another region. You can let KMS create keys for you or you can import your own.

You can define key usage and management permissions separately. The idea is that the person who can revoke/remove keys isn't able to decrypt/encrypt data with it and vice versa.

If you want to delete KMS keys, you can only schedule the removal which will kick in within 7-30 days (at random).

KMS is basically shared HSM where you get a section of it as opposed to dedicated Cloud HSM.

# EXAM
* Both KMS and CloudHSM let you create, store & manage your (symmetric & asymmetric) encryption keys. Both are well supported and highly available.
* KMS is on shared HW
* KMS is FIPS 140-2 compliant (to level2! CloudHSM is Level 3 & has more features)
* Not capable of interacting via the industry standard methods, only via AWS API (see CloudHSM)
* KMS DOES NOW support symmetric & asymmetric keys (before it didn't support PKI)
* Great if you don't have any regulatory requirements to be on dedicated HW.
* Great if you need to integrate with other AWS services!! CloudHSM couldn't do that
* Great if your keys need to be rotated!! With CloudHSM you build that manually
* Great if you need separation between key administrators and key users!! With CloudHSM you couldn't do that (I think)

## KMS Envelope Encryption
* Data key, envelope key, data encryption key are all terms for one and the same thing! Great, I'm going to call it DEK, Data Encryption Key!
You use your Customer Master Key to create & encrypt a DEK which encrypts your data. You're encrypting the key that encrypts your data with the CMK (Customer Master Key).
(Customer) Master Key -> DEK -> Data
### The Customer Master Key (CMK):
* Consists of:
  * Alias (needs to be created, keys per default don't have one)
  * creation date
  * Description
  * Key state (enabled/disabled)
  * key material (provided by AWS or customer)
  * Can NEVER be exported!! If you need to export your key you will need to use CloudHSM (Hardware Security Module) because HSM is dedicated hardware for you and not shared infrastructure.
* Is used to en/decrypt the DEK
* CMKs come in two variants, AWS managed CMKs and customer managed CMKs (manually created key, not created by AWS unlike alias/aws/s3)
#### Key Rotation
* AWS managed CMKs rotate every 3 years and this can't be disabled.
* Customer managed CMKs rotate every year but this can be disabled.
* These rotated out CMKs are what's called backing keys. They become disabled and won't be used for encryption any more but are needed for decryption

### DEK:
  - Is used to encrypt and decrypt the data.

## KMS Setup:
### Create CMK & Alias
* Create CMK with an optional description (aws kms create-key)
* [OPTIONAL] Create Alias (needs to be created, keys per default don't have one) (aws kms create-alias)
### Secure CMK
* Define the keys "Administrative Permissions"
  * IAM users/roles that can administer but not use the KMS keys
* Define "usage permissions"
  * IAM users/roles that can use but not administer the KMS keys
* KMS is great because of this! I can give somebody full admin into S3 and they still can't decrypt the data!
* Can be used via PrivateLink and thus in completely isolated VPCs
### Create, use and manage DEK
* Create DEK (aws kms generate-data-key --key-id alias/foo)
  * This returns 2 base64 encoded copies of the DEK, 1 plain text version and 1 encrypted version of the DEK (encrypted with the CMK specified).
  * KMS doesn't keep the DEK. You need to store the encrypted DEK with the encrypted data and present both back to KMS to decrypt via the CMK
* Decrypt the DEK (aws kms decrypt --ciphertext-blob $ENCRYPTED_DEK)
  * Use the output of that (the base64 encoded key) to decrypt the data
* Optional Re-encrypt (aws kms re-encrypt)
  * KMS decrypts the encrypted data (text, not an EBS volume) with the existing CMK and re-encrypts it with a new CMK. In this whole process you don't get to see the plain-text data, AWS handles this all for you.
