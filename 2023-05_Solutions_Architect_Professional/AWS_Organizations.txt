Allows you to manage multiple AWS Accounts at once. You can group accounts into Organizational Units and apply policies (Service Control Policies) to these accounts.

Used to just be "consolidated billing" and is still the default. The other option is "all features" and gives you SCPs, Resource Sharing (RAM), etc. This can be changed after creation.
https://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/useconsolidatedbilling-discounts.html
* A Master account can be put under an OU in the organization however any SCP applied to the account simply won't take effect. Another good reason not to have any resources in the master account
* With SCPs, if you apply multiple SCPs to an account, only the OVERLAP of the two SCPs is what's allowed. For that reason, the default SCP on a root account/OU, that gets inherited all the way down the tree, is an explicit allow all. So an SCP doesn't actually grant any permissions, it just tells you what permissions could be granted to the account with an explicit allow. If you remove the explicit allow or place an explicit deny there, anything in that AWS account, including the root account, won't have the permission you've removed.

# EXAM
* Centrally manage accounts
* Apply SCPs to the accounts. E.g: You have a number of AWS Accounts and want to restrict access to EC2 in all of them. Make them part of an organization, apply SCP to it.
* Manage automatic account creation
* Consolidated billing
* Volume Discounts!
