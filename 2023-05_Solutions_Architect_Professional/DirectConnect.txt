# EXAM
* Private, secure, high throughput connection into AWS over AWS' backbone network.
* Lets you access AWS' public as well as your private (VPC) resources in AWS.
* Exam question: What can you do if the VPN keeps dropping?
* You can connect one DX to up to 10 accounts within your AWS Organization, called Multi-Account DX. You can share the same DX Gateway across accounts.
* DXs are quicker and more reliable because you're not taking the scenic route over who knows how many different routers at various levels of traffic going over them, you're going over your own equipment in a direct path straight into AWS
* DX is cheaper if you're transferring larger datasets because the data over an enterprise dedicated connection is cheaper than what an ISP will give you.
* Takes time to set up, weeks even months if you need to get equipment into the DC that does the network hand-off.
* DX connections are not encrypted by default. It's your own wire, so it's not necessarily needed.
  * If you do need encryption though, VGWs work in AWS' Public Zone, just like S3 etc, since it's got a public IP address to route traffic to. So you can create a VPN over DX with a public VIF, and get the benefits of the DX connection (reliability, low latency due to direct route) and have encryption of your traffic.
  * Furthermore, using this technique you'd get around the limitation of only being able to create private VIFs in the same region. You could create a VPN connection over the private VIF to other AWS regions.
* A DX connection goes into a specific region, and in the past, you could create only (public & private) VIFs in that region.
* If you create multiple DX in the same region AWS will connect you to a separate router and you get better availability through redundancy
* https://aws.amazon.com/answers/networking/aws-multiple-data-center-ha-network-connectivity

## Pay $$$
* For every hour the DX port is provisioned at AWS (on their router) there is a port charge, depending on 1G or 10G of course
* You're charged for DTO not data transfer in (but DTO is still a lot lower than public internet charges) Viable above Terra Bytes/month

## AWS Network zones
* AWS distinguishes their private network, i.e. VPCs that you or AWS occupies.
* And what they call AWS public Zone which is basically their network of all publicly available AWS endpoints. Creating a DX with a public VIF, AWS sends you all these public endpoints via BGP to your router. It doesn't mean that you get public internet access via a public VIF, it just means you can reach AWS' public endpoints.

## Public & Private VIFs
* Each VIF operates on a VLAN between 1 and 4094, needs to be private OR public and it needs BGP information like an ASN number
* Public VIFs used to cover only the region that you're DXing into and you couldn't reach public endpoints such as DynamoDB or S3 in other regions. They've since changed that and you can reach all their public endpoints, of course you're going via AWS backbone when you do cross regions which gives you better latency etc as well.
* Private VIFs are associated with a VGW (Virtual Private Gateway) and are thus bound to a specific VPC (and region).
* Private VIFs build a 1-to-1 relationship between a VLAN in your on-prem DC and a VPC in AWS. This can be quite the admin overhead if you're setting up 100s of VIFs/connections. See DX GW.

## Direct Connect Gateway
* It's a GLOBAL service and you can associate private VIFs with DX GW
* These private VIFs can then be associated with VGW in *ANY* region.
* So now you're maintaining a single private VIF from on-prem to the DX GW and then you can connect private VIFs into any region
* DX GW advertises all connections it's aware of over BGP, reducing the admin overhead greatly! AWS manages the connection from DX GW to any VGW in any region.
* DX GW is NOT transitive. If I connect my on-prem DC with 2 regions, on-prem can talk to both regions however the two regions (VPCs in those regions) can't talk to each other! (look at Transit GW, that's transitive).

## Ordering a DX from AWS
As long as you want a 1G or 10G fibre connection from AWS, you can order it straight from the console as long as you fulfill the requirements:
### Requirements:
* The connection needs to use "single mode fiber"
* Router needs to support
  * BGP
  * BGP MD5 authentication
  * VLANs 802.1q
* Then you can download your Letter of Authorization and Connecting Facility Assignment (LOA-CFA). Authorisation to connect to the AWS port
## Ordering a DX from a partner
If you have the need for a <1G connection, you can order that from a partner and you'll get what's called a hosted connection.
* hosted connections need their peering approved in your account, but technically it's the same under the hood.
