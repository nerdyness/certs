* SQL Engine overlaid on S3 based on Apache Presto
* Query raw data as it sits in an S3 bucket
* Convert your CSV file to Parquet format to get a big performance jump
  * Supports Amazon Ion, Apache Avro, Apache Parquet, Apache WebServer logs, CloudTrail logs, CSV/TSV/xSV, JSON, ORC, LogStash
* Similar concept to RedShift Spectrum but w/ RedShift Spectrum, you will want to join existing tables from RedShift w/ data in S3 where as Athena is used if all your data lives self-contained in S3 w/o the need to join w/ other data sources.

* You can have structured, semi-structured or even unstructured data in S3 and Athena will create a schema from it
* This schema is then overlayed over the top of the data to give you access to the data
  * This is called schema on read, you don't modify the data but read it through a schema
  * This schema is stored in a data catalog
* You don't need to do any ETL!
* No servers, you only pay for the data your queries read!
  * ~$5/GB (you can optimise by compressing or moving to Parquet or some other columnar storage)
