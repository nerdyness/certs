# Terminology

* Persistent data stores | Durable | Data survives reboots etc | RDS, Glacier, S3
* Transient data stores | Data is just passing through | SQS, SNS
* Ephemeral data stores | Data is lost when stopped | EC2 instance store, memcached

* IOPS -> measures how fast you can read/write from/to a device (think of a race car)
* Throughput -> Measures how much data can be moved at any one time (think of a truck)
  * The race car is going to get there faster however the truck can deliver more to the other end in one go.

# Consistency models
* ACID -> Atomic, Consistent, Isolated, Durable
* BASE ->
  Basically Available - values are available, even if they are stale
  Soft-state - might be instantly consistent across stores
  Eventually Consistent - will achieve consistency at some point
