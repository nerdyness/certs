# Cross-Zone Load balancing
* When you create an LB it creates a LB node (an ENI) in each registered subnet
* With cross-zone load balancing disabled, each LB node would only send traffic to the healthy instances in it's AZ
* Once enabled, each LB node can also send traffic to nodes in a different AZ
* Handy if you have 2 instances in us-east-1a and 20 instances in us-east-1b.

# Listener Rules
## Path pattern routing
## Host pattern routing

# EXAM
# Monitoring types
* CloudWatch Metrics
* Access Logs
* Request Tracing -> ALB adds the X-Amzn-Trace-Id header before passing the request to your infrastructure. Only supported for ALB!
* CloudTrail Logs

* ELB Access Logs can store data from already terminated EC2 instances
* AWS can pre-warm an ELB for you if you expect a sudden increase in traffic (i.e. 5 or 10-fold of your normal traffic. Tell us: Start/End date, Requests per second AND avg request size)
* "Dualstack" refers to using IPv4 & IPv6 on your ALB

# 3 types:
* application load balancer
  - Preferred for HTTP/S, Layer 7 load balancer
  - Is application-aware, can handle advance requests such as if language NO then route to Norway
  - Is intelligent, advanced request routing
* classic load balancer
  - Classic Layer 4 load balancer
  - Can also use Layer 7 specific features like XFF headers, sticky sessions.
  - Cheapest
* network load balancer
  - Operating at level 4 (Transport Layer)
  - Where extreme throughput is required (millions of requests/second with super low latency)
  - Expensive
  - If you see a question on LBs and static IPs, it's the NLB. NLBs give you a static IP in the AWS AZ/subnet.
  - You can put an ALB behind an NLB to get the best of both worlds.

# HTTP Error Codes (really?!)
2XX -> still good
3XX -> redirects
4XX -> client error | 5xx -> server side error
400 Bad/malformed request
401 Unauthorized request
403 Forbidden
404 Not Found
460 Client closed connection before Load Balancer could respond, client timeout could be too short
463 Loadbalancer received a request with >30 IPs in the X-Forwarded-For header, redirect loop? kill it
500 internal server errror
502 Bad gateway (closed connection, malformed response...)
503 Service unavailable (no registered targets)
504 Gateway Timeout
561 Unauthorized, received an error from the IdP while trying to authenticate users

# Load balancer metrics
* ARE PUBLISHED EVERY 60 SECONDS TO CLOUDWATCH BY DEFAULT
## Health
* HealthyHostCount
* UnhealthyHostCount
* HTTPCode_Backend_2XX
## Performance
* Latency -> of the registered instances
* RequestCount -> # completed requests per time interval
* SurgeQueueLength -> Number of pending requests (max queue length is 1024) CLASSIC ONLY
* SpillOverCount -> Number of requests rejected due to exeeded SurgeQueueLength CLASSIC ONLY

## IS THIS NEEDED?? ## * Read the FAQ prior to the exam
## IS THIS NEEDED?? ## * If you do see the word "Elastic Loadbalancers" they will typically mean classic ELBs, the new ones are too new
## IS THIS NEEDED?? ## * Instance can be inService our OutofService
## IS THIS NEEDED?? ## * Healthchecks need to be defined
## IS THIS NEEDED?? ## * Always use the name of the ELB, never the IP, it changes :)
## IS THIS NEEDED?? ## * 504 Error means the gateway has timed out. The app ain't responding within the idle timeout period. Troubleshoot the app. Is it the DB? The Web servers?
## IS THIS NEEDED?? ## * The XFF (X-Forwarded-For) Header is set with the IP of your client
## IS THIS NEEDED?? ## * You need >=2 public subnets for an internet facing LB
