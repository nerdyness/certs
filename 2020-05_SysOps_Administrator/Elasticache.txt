Elasticache is a web service that makes it easy to deploy, operate and scale an in-memory cache in the cloud. The service improves the performance of web applications by allowing you to retrieve information from fast, managed, in-memory caches, instead of relying entirely on slower disk-based databases.

* ElastiCache supports 2 caching engines, Memcached and Redis.
* 2 different caching strategies: Lazy loading and write through (and TTL, technically)
* Elasticache Node failures are not fatal, just lots of cache misses
* Cache miss penalty: Initial request, query database, writing to cache
* Avoid stale data by implementing a TTL

# Redis vs Memcached
Use Memcached if
* Object caching is your primary goal
* You want to keep things as simple as possible
* You want to scale your cache horizontally (multiple nodes)

Use Redis if
* You have advanced data types such as lists, hashes and sets
* You are doing data sorting and ranking such as leader boards
* Data persistence is important
* Multi-AZ is important
* Pub/Sub capabilities are needed

# Stressed?
Common exam question will be "Your DB is overloaded, what can you do to sort this out?" - Add a read replica, add Elasticache, don't run OLAP on your OLTP DB, Maybe use Redshift.

# EXAM
* If you're using Memcached (which is multi-threaded, Redis isn't) you want to scale out once your cluster hits a load of 90%.
* There are 4 monitoring options to look at: CPU Utilisation | Swap Usage | Evictions | Concurrent Connections
* If you are using Memcached as your caching engine, adjust Memcached_Connection_Overhead parameter when the overhead pool is less than 50MB?

## Evictions
* There is no recommendation for Redis or Memcached, pick a threshold based on your application.
* Memcached: You can either scale up (add more memory to existing nodes) or scale out (add more nodes)
* Redis: You can only scale out (add read replicas)

## Concurrent connections
* There is no recommendation for Redis or Memcached, pick a threshold based on your application.
* If you see a spike in concurrent connections it can be a spike in users or your app isn't releasing connections as it should.
* Set a CW alarm for # concurrent connections for Elasticache!
