# EXAM
*------ S3 is hugely important as it's been around for so long, a lot of services use it.
* Read the FAQ https://aws.amazon.com/s3/faqs/
* S3 is object based storage. Not block-based. You can upload files/objects.
* <=5TB file size. Minimum size is 0 bytes!!
*
## DO I NEED THIS? ## * You can have 100 buckets per account by default.
* The largest file you can upload in a single PUT is 5GB, after that you need to do a multi-part upload. Highly recommended for files >100MB though.
  * The objects will be split into chunks and uploaded in parallel so it speeds up your connection too!
* Unlimited storage (AWS will re-provision if it runs out)
* Bucket names are in a universal name-space, scope across all of AWS!! however a bucket is in a particular region
* By default, all buckets and objects stored inside them are private. Even a bucket that has read rights enabled doesn't mean that the object inside it has read permissions.
* There is a cool API/function call to create a unique bucket name
* It can be that they ask you the format of a S3 bucket URL, https://s3-$region.amazonaws.com/$bucket
* For static website hosting, the URL is http://$bucket.s3-website-$region.amazonaws.com/
* After uploading a file you will receive a HTTP/200 status code.
* Data consistency model for S3...
  - Read after write consistency for PUTS of new objects. Meaning if you upload something new you're going to be able to read that object straight away
  - Eventual consistency for overwrite PUTS and DELETES (takes time to propagate). It's designed across multiple data centers thus an update take time.
* Updates are atomic, so as you query you either get the old or the new version, never anything corrupt.
* S3 is a simple key/value store. Each entry/object consists of
  - key = name of the object
  - value = data/content
  - Version ID
  - Metadata (Timestamp, last update).
  - Sub resources (bucket specific configuration)
    - Access Control Lists
    - Bucket policies
    - CORS (Cross Origin Resource Sharing)
    - Transfer acceleration
    - BitTorrent (Add ?torrent to the S3 GET request, that will create the torrent file which takes time depending on how large the file is so run the first ?torrent yourself) - Can significantly reduce (bandwidth & request) cost for bucket owner.

* Lifecycle mgmt - Storing data that's x-days old in a different tier
* Lifecycle rules are based on the object creation date
* Lifecycle mgmt hints and tips for the exam:
  - Can be used in conjunction with versioning
  - Can be applied to current and previous versions of an object
  - Supports the following actions:
    - Transition to "Standard Infrequent Access" class (you can only copy objects in there if object >128kb in size and >=30 days after it's been created)
    - Transition to Glacier (lifecycle mgmt can only copy things after it's been 30 days in the current storage class)
    - Delete object
* Transfer Acceleration is a service where Amazon accepts data into a bucket from one of their edge location and then route it via their pipes to the region in which your bucket is provisioned.

* Tiered Storage Available
  - Standard - stored redundantly across multiple devices and facilities, designed to sustain the loss of 2 facilities concurrently.
  - Standard-IA - Infrequently Accessed data which still requires rapid access when needed. Lower fee than S3 but incurrs charge a retrieval fee.
  - Intelligent Tiering - AWS charges a small fee to monitor your access patterns. no access for >=30 days, object goes into IA. If it's accessed, it's automatically moved back.
  - One-Zone IA - Same as IA, but stored in only one AZ (20% less than regular S3 cost) Used to be called - Reduced Redundancy Storage (RRS).
  - Glacier - Very cheap, but only used for archival. Takes 3-5 hours to restore from Glacier.
  - Glacier Deep Archive - Cheapest, designed for regulatory industries where docs have to be stored 7-10 years. Retrieval in 12 hours
Category                  | S3 Standard     | Standard - IA    | Intelligent Tiering | One-Zone IA      | S3 Glacier       | Deep Archive     |
--------------------------+-----------------+------------------+---------------------+------------------+------------------+------------------+
Durability                | 99.99999999999% | 99.99999999999%  | 99.99999999999%     | 99.99%           | 99.99999999999%  | 99.99999999999%  |
Availability              | 99.99%          | 99.99%           | 99.95%              | 99.95%           | N/A              | N/A              |
AZs                       | >=3             | >=3              | >=3                 | 1                | >=3              | >=3              |
Minimum charge per object | N/A             | 128KB            | N/A                 | 128KB            | 40KB             | 40KB             |
Minimum storage duration  | N/A             | 30 days          | 30 days             | 30 days          | 90 days          | 180 days         |
Retrieval fee             | N/A             | per GB retrieved | N/A                 | per GB retrieved | per GB retrieved | per GB retrieved |
First byte latency        | Milliseconds    | Milliseconds     | Milliseconds        | Milliseconds     | minutes/hours    | hours            |

Charged for:
  - Amount of Data
  - Number of Requests
  - Storage management pricing. Add tags to identify costs per department, tags are being charged for. Individual objects do not inherit bucket tags.
  - Data coming into S3 is free, but bucket to bucket replication costs.
  - Transfer Acceleration
  - Cross-region replication

* Once versioning is turned on it can only be disabled, not removed. You'd have to create a new bucket to remove it.
* Deleting a version means it's gone. Deleting an object will mark it as deleted and keep the previous versions of it. In fact, to restore, you delete the version that says "deleted" :)
* You can set versioning to require MFA for a delete.
  * Once enabled, you will need an MFA code for deletes as well as suspending/reactivating versioning.
* For cross region replication to work (bucket to bucket, can even be a different AWS account), both buckets need versioning turned on.
  * You can't replicate to another bucket in a different account in the same region using cross region replication.
* With cross region replication you can change the storage class in your destination bucket to say "standard IA" for instance.
* Turning on cross region replication does not copy existing objects across, only new objects/changes will be replicated. Use the command line to copy objects (won't sync permissions per default, use --acl). New objects have permissions synchronised.
* Deleting the deletion marker or ANY OTHER VERSION does not replicate across to the destination bucket!! I guess it's a backup cautious thing. Better to have the version than not have it. Just weird that deleting a delete marker does not replicate.

## Securing your buckets
* You can set up access control using Bucket Policies (bucket-wide scope) and Access Control Lists (can apply to individual objects)
* You can configure S3 buckets to create access logs for all access requests (will be logged to another bucket can be in another account)

## Encryption
* In transit (when sending to/from) done via SSL/TLS
* At rest:
  - Client side (prior to upload)
  - Server Side Encryption with Amazon managed keys (SSE-S3)
  - Server Side Encryption with KMS (SSE-KMS)
  - Server Side Encryption with Customer provided keys (SSE-C)
* You can even set a bucket policy to prevent unencrypted files from being uploaded by creating a policy that only allows requests which include the x-amz-server-side-encryption parameter in the request header.

## Transfer Acceleration
S3 Transfer acceleration utilises the CloudFront Edge Network to accelerate your uploads to S3. Instead of uploading directly to your S3 bucket, you can use a distinct URL to upload to an edge location which will then transfer that file to S3. Example: $bucketname.s3-accelerate.amazonaws.com

## Pre-signed URLS
* You can access/share objects using pre-signed URLs
* These expire after some time (default 1h, max 1 week) --expires-in #OfSeconds

## Bucket policies and wildcards
* Bucket-level permissions usually contain the word bucket in them, e.g.: s3:CreateBucket, DeleteBucket, ListBucket...
  * They only need to be applied to the "arn:aws:s3:::bucket-name" not "name/*"
* Object-level permissions usually contain the word object in them, e.g.: s3:PutObject, DeleteObject, ...
  * They need to be applied to the "arn:aws:s3:::bucket-name/*"
## Bucket policies and IP restrictions
* You can allow/deny bucket access based on IP address
* You use IpAddress and NotIpAddress as follows:
"Condition": {
  "IpAddress": {"aws:SourceIp": "10.1.1.1/32" },
  "NotIpAddress": {"aws:SourceIp": "10.2.1.1/24" }
}
