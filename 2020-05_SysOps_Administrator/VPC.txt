Virtual Private Cloud "A virtual Datacentre in the cloud"

Every region has a default VPC which you get when your account is created

* You can have multiple VPCs per region (soft-limit of 5)
* Only <=1 Internet GW (IGW) per VPC
* Biggest subnet (& VPC) you can get is [10.0.0.0]/16
* Smallest subnet (& VPC) you can get is /28
* Ephemeral ports in AWS are 1024-65536

# What can you do with a VPC?
* Launch instances into a subnet of your choosing
* Assign custom IP Address ranges in each subnet
* Configure routes between subnets
* Create an Internet Gateway (They're highly available across all AZs)
* Improved security controls
  - subnetting
  - NACLS (Network Access Control Lists)
  - Instance security groups (can span across AZs, not across VPCs!)

# Default VPC vs Custom VPC
* Default VPC is user friendly and allows you to deploy instances immediately
* All subnets in the default VPC have a route to the Internet
* Each EC2 instance has a public & private EIP assigned

# VPC Peering
* Allows you to connect 2 VPCs via a direct network route using private IP addresses
* Instances behave as if they're on the same private network when using VPC peering
* You can peer VPCs across accounts
* VPC peering isn't transitive, meaning if you have VPC-A <-> VPC-B <-> VPC-C then VPC-A can't talk to VPC-C, you have to peer those 2 as well.
* Your subnets can't overlap

# EXAM
## Stupid terminology that will only turn up on a cloud guru
* target = 0.0.0.0/0 or similar
* destination = igw-xxxxxx or similar


* One subnet = One AZ, they can't spread across AZs!
* A security group can span VPCs.
* You can only have 1 internet gateway/VPC (obviously :) )
* No transitive peering
* When you create a subnet you obviously don't get the network and the broadcast address, but AWS also use the first available address for their router, then the second for DNS and the third for future use, so you loose 5 IPs in every subnet
* A VPC Consists of IGWs, VPGs, Route Tables, NACLS, Subnets, Security Groups
* When you create a new VPC it contains a default route table, "default" NACLs and a default security group.
  * It won't create any subnets, nor will it create a default IGW
* us-east-1a in your account my be different to us-east-1a in my account. Randomizzzed.
* You are allowed to do your own vulnerability scans in your own VPC without alerting AWS first. Just not external-in or in other people's stack I guess.

## ELBs
* You need >=2 public subnets for an internet facing LB

## Flow logs
* Can be created at VPC, Subnet or ENI level!
* Will be logged into CloudWatch or an S3 bucket.
* You can only enable flow logs on peered VPCs if the peer is in your Account
* You can't tag flow log (configuration)
* Once you created a flow log (configuration) it can't be changed (like IAM role etc).
* Not all traffic is monitored, the following exceptions aren't logged. Traffic to
  * AWS' DNS servers (Custom DNS is)
  * AWS' windows license activation
  * 169.254.169.254/
  * DHCP
  * AWS' reserved IP addresses for the default VPC router.

## Bastion
* You can get special/hardened AMIs from AWS for Bastion hosts.

## VPC Endpoints
### Interface Endpoint
* Is an ENI in your VPC with a private IP offering a private entrypoint for supported services. (Powered by PrivateLink) You basically don't leave the AWS network to get to i.e.: S3.
* You connect an ENI to an EC2 instance and that let's you communicate to services privately.
* These are virtual devices that scale horizontally, are HA, redundant and don't constrain your bandwidth.
### Gateway Endpoint
* Are similar and only support S3 and DynamoDB (atm)
* You provision a Gateway (like NAT or IGW) and that gets (automatically albeit delay) added to your route table.
* You can then access S3 or DynamoDB privately (but you may have to specify the --region which you didn't have to before)

## NACLS
* Security groups are stateful, NACLS are stateless!
* NACLs live in 1 VPC & are associated to subnets (1 NACL can be associated to many subnets)
  * A new NACL has a default DENY as the first in & outbound rule (the default NACL all in & outbound traffic).
* NACLs are evaluated prior to security groups in a VPC
* Each subnet needs a NACL associated to it, if you don't associate it to any custom NACL the "default" NACL is used.
* NACLs have denies so you can block IP addressses, SG's ain't got that.
* NACLs have two (inbound/outbound) numbered list of rules that are evaluated in numerical/chronological order.

## NAT instances
* When creating a NAT instance, disable the source/destination check on the instance
* NAT instance/GW must be in a public subnet
* There must be a route from the private subnet to the NAT GW/Instance for this to work
* The network throughput an EC2/NAT instance can do depends on the instance size. Increase instance size if throughput becomes an issue
* You can create HA using autoscaling groups, multiple subnets in different AZs and a script to automate fail-over.
* All instances are always behind a security group - you gotta allow stuff in/out.
### NAT Gateways (preferred because easier and better)
* Are redundant inside the AZ
* Starts at 5Gbps and scales to (currently) 45 Gbps
* NAT Gateways aren't IPv6 compatiple - use an Egress-only IGW instead.
* No need to patch the OS, disable source/destination checks or tinker with security groups
* Automatically get a public IP address
* You still gotta update your route table though
* HA by creating multiple NAT GWs, one in each AZ.

## VPNs
* You need to create a VGW - Virtual Private Gateway in your VPC (you'll need one for DX as well as VPN connections)
* There are two tunnels which connect to the CGW - Customer Gateways. These are usually physical appliances in your DC that establish the VPN connection into AWS.
  * Two tunnels for redundancy, you don't need to connect both but it's recommended.
* Because you have more than one CGW, you'll need to route traffic to the CGW, not the VGW
* A CGW is basically a static route or a BGP ASN to the public IP in your DC. You'll need to create this in the AWS Console
